int count = 0;
float x = 0.0;
float y = 0.0;
float w = 10.0;
float t = 0.0;
float t1 = 0.0;

int num = 20;

void setup() {
  size(600, 600);
  background(255, 255, 0);
}

void draw() {
  background(255, 255, 0);

  println("hello world : " + count);
  count = count + 1;

  //fill(255, 0, 255);
  //stroke(0);
  noFill();
  stroke(0);
  //rect(x-w/2.0, mouseY, 100, count);
  ellipse(x, y, w, w);

  if (x > width - w/2.0) {
    x = 0.0;
  }
  
  x = x + 1.0;
  y = height/2 + sin(t)*200;
  w = 20 + cos(t1)*20;
  
  t = t + 0.032;
  t1 = t1 + 0.25;
  
  float cellSizeX = width/float(num);
  float cellSizeY = height/float(num);
  
  for(int j=0; j<num; j++){
    for(int i=0; i<num; i++){
      //println(i);
      rect(i*(cellSizeX), j*(cellSizeY), cellSizeX, cellSizeY);
      ellipse(i*(cellSizeX)+(cellSizeX)/2, j*(cellSizeY)+(cellSizeY)/2, cellSizeX-5, cellSizeY-5);
    }
  }
}