float x = 100;
float speed = 1;

void setup(){
  size(600, 600);
  background(0);
}

void draw(){
  background(0);
  
  fill(map(x, 100, 500, 0, 255));
  ellipse(x, height/2, 50, 50);
  x = x+speed;
  
  if(x > 500){
    x = 500;
    speed = -1;
  }
  if(x < 100){
    x = 100;
    speed = 1;
  }
  
}