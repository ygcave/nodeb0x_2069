float w = 300;
int velocity = 1;

void setup(){
  size(600, 600);
}

void draw(){
  background(0);
  ellipse(width/2, height/2, w, w);
  
  w = w+velocity;
  
  if(w < 300){
    w = 300;
    velocity = 1;
  }
  
  if(w > 500){
    w = 500;
    velocity = -1;
  }
  
  
}