int totalNum = 10;
int cols = 20;
int rows;
boolean mode = false;

float maxAngle = 0.0;
float maxAngleSpeed = 5.0;

void setup() {
  size(800, 800);
  background(0);
  rows = cols;
}

void draw() {
  background(0);
  
  totalNum = int(map(mouseY, 0, height, 2, 50));

  for (int y = 0; y<rows; y++) {
    for (int x = 0; x<cols; x++) {
      for (int i=0; i < totalNum; i++) {
        //float w = 200-i*20;
        pushMatrix();
        translate(x*(width/cols) + (width/cols)/2, y*(height/rows) + (height/rows)/2);

        float angle = 0;

        /*if((x%2==0 && y%2==0) || (x%2==1 && y%2==1)){
         angle = -map(i, 0, 9, 0, maxAngle);
         }else{
         angle = map(i, 0, 9, 0, maxAngle);
         }*/
        if ((x+y)%2 == 0) {
          angle = -map(i, 0, 9, 0, maxAngle);
        } else {
          angle = map(i, 0, 9, 0, maxAngle);
        }

        rotate(radians(angle));
        float w = map(i, 0, totalNum-1, width/cols, 10);

        if (mode == true) {
          stroke(255);
          noFill();
        } else {
          noStroke();
          float c = map(i, 0, totalNum-1, 0, 255);
          fill(c);
        }
        rect(-w/2, -w/2, w, w);
        popMatrix();
      }
    }
  }
  
  maxAngle = maxAngle + maxAngleSpeed;
  if(maxAngle > 360){
    maxAngle = 360;
    maxAngleSpeed = maxAngleSpeed*(-1);
  }
  if(maxAngle < 0){
    maxAngle = 0;
    maxAngleSpeed = maxAngleSpeed*(-1);
  }

  //translate(width/2, height/2);
  //stroke(255, 255, 0);
  //line(0, -height/2, 0, height/2);
  //line(-width/2, 0, width/2, 0);
}

void keyPressed(){
  if(key == 'm'){
    mode = !mode;
  }
  if(key == ' '){
    saveFrame("myWork_####.jpg");
  }
}